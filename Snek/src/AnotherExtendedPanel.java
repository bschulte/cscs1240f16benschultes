import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class AnotherExtendedPanel extends JPanel{

	public static int randomWithRangeX(int min, int max)
	{
	  int range = (max - min) + 1;     
	   return (int)(Math.random() * range) + min;
	}
	
	public static int randomWithRangeY(int min, int max)
	{
	  int range = (max - min) + 1;     
	   return (int)(Math.random() * range) + min;
	}
	
	public void paintComponent(Graphics gr){
		super.paintComponent(gr);
		gr.fillRect(randomWithRangeX(0,3840), randomWithRangeY(0, 2160), 100, 100);
		
	}

	public Dimension getPreferredSize(){
		Dimension dim = new Dimension(3840,2160);
		return dim;
	}


}
