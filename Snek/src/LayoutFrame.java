import java.awt.event.KeyEvent;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.KeyAdapter;
import javax.swing.JFrame;

public class LayoutFrame {
	static boolean up = false;
	static boolean down = false;
	static boolean left = false;
	static boolean right = false;
	
	final static int MAX_WIDTH = 500;
	final static int MAX_HEIGHT = 500;
	
	public static void main(String [] args){
		ExtendedPanel panel = new ExtendedPanel();
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	//	frame.setContentPane(new LayoutGrid());
		frame.pack();
		frame.setVisible(true);
		
		frame.addKeyListener(new KeyAdapter()
		{
			public void keyPressed(KeyEvent e)
			{
				if(e.getKeyCode()==KeyEvent.VK_UP)
				{
					up = true;
				}
				else
				if (e.getKeyCode()== KeyEvent.VK_DOWN)
				{
					down=true;
				}
				else 
				if(e.getKeyCode()== KeyEvent.VK_LEFT)
				{
					left = true;
				}
				else
				if(e.getKeyCode()== KeyEvent.VK_RIGHT)
				{
					right=true;
				}
			}
			
			public void keyReleased(KeyEvent e) {
				if(e.getKeyCode()== KeyEvent.VK_UP)
				{
					up = false;
					down = false;
					left = false;
					right= false;
				}
				else
				if (e.getKeyCode()== KeyEvent.VK_DOWN)
				{
					up = false;
					down = false;
					left = false;
					right= false;
				}
				else 
				if(e.getKeyCode()== KeyEvent.VK_LEFT)
				{
					up = false;
					down = false;
					left = false;
					right= false;
				}
				else
				if(e.getKeyCode()== KeyEvent.VK_RIGHT)
				{
					up = false;
					down = false;
					left = false;
					right= false;
				}
			}
});
		Timer timer = new Timer();
		TimerTask task= new TimerTask() {
			
			public void run() {
				if (up) {
					panel.setDir(0);
				}
				else if (down) {
					panel.setDir(1);
				}
				else if (left) {
					panel.setDir(2);
				}
				else if (right) {
					panel.setDir(3);
				}
				


				panel.move();
				
				frame.repaint();
			}
		};
		timer.schedule(task, 0, 120);
			
	}
	

}
