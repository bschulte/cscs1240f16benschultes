import javax.swing.JFrame;
import list.Item;
import javax.swing.*;
import java.awt.font.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
class SolutionRunnable implements Runnable, MouseListener {
public void setContentPane(){
	
}

	public void run(){
		
		JFrame frame = new JFrame();

		FlowLayout flow = new FlowLayout();
		frame.setLayout(flow);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
		frame.getContentPane().setBackground(Color.white);



		Item anItem= new Item(new Term(3.7, new Variable("tre", 37.4), 2.0));
		for( Integer count=0; count<3; count++){
			anItem.insertAfter( new Term(6.2, new Variable("ert", 96.4), 3.0));
		}
		
		Item beItem= new Item(new Term(9.4, new Variable("tre", 4.4), 4.0));
		for( Integer count=0; count<3; count++){
			beItem.insertAfter( new Term(3.2, new Variable("ert", 6.8), 8.0));
		}
		Font aFont = new Font("Times New Roman", Font.PLAIN, 50);

		TermLabel coeff;
		TermLabel var;
		String expo;
		TermLabel exp;
		TermLabel plus;
	
		TermLabel equal = new TermLabel("=");
		equal.addMouseListener(this);
		equal.setOpaque(true);
		equal.setFont(aFont);
		
		anItem = anItem.first();
		do{
			coeff = new TermLabel(((Term)(anItem.item)).coefficient.toString());
			var = new TermLabel(((Term)(anItem.item)).variable.name);
			expo = ((Term)(anItem.item)).exponent.toString();
			exp = new TermLabel ("<html><sup>" + expo + "</sup></html>");
			plus =  new TermLabel(" + ");
			frame.add(coeff);
			frame.add(var);
			frame.add(exp);
			coeff.setFont(aFont);	
			var.setFont(aFont);
			exp.setFont(aFont);
			plus.setFont(aFont);
			plus.setBackground(Color.WHITE);
			plus.addMouseListener(this);
			plus.setOpaque(true);
			plus.setFont(aFont);
			exp.addMouseListener(this);
			exp.setOpaque(true);
			exp.setBackground(Color.white);
			exp.setFont(aFont);
			var.addMouseListener(this);
			var.setBackground(Color.white);
			var.setOpaque(true);
			var.setFont(aFont);
			coeff.addMouseListener(this);
			coeff.setBackground(Color.white);
			coeff.setOpaque(true);
			coeff.setFont(aFont);
			equal.setBackground(Color.white);
			if(anItem.next != null){
				frame.add(plus);
			}else{
				frame.add(equal);
			}
			anItem = anItem.next;
		}while( anItem != null);{
			
		}
		
		do{
			coeff = new TermLabel(((Term)(beItem.item)).coefficient.toString());
			var = new TermLabel(((Term)(beItem.item)).variable.name);
			expo = ((Term)(beItem.item)).exponent.toString();
			exp = new TermLabel ("<html><sup>" + expo + "</sup></html>");
			plus =  new TermLabel(" + ");
			frame.add(coeff);
			frame.add(var);
			frame.add(exp);
			coeff.setFont(aFont);	
			var.setFont(aFont);
			exp.setFont(aFont);
			plus.setFont(aFont);
			plus.setBackground(Color.WHITE);
			plus.addMouseListener(this);
			plus.setOpaque(true);
			plus.setFont(aFont);
			exp.addMouseListener(this);
			exp.setOpaque(true);
			exp.setBackground(Color.white);
			exp.setFont(aFont);
			var.addMouseListener(this);
			var.setBackground(Color.white);
			var.setOpaque(true);
			var.setFont(aFont);
			coeff.addMouseListener(this);
			coeff.setBackground(Color.white);
			coeff.setOpaque(true);
			coeff.setFont(aFont);
			equal.setBackground(Color.white);
			if(beItem.next != null){
				frame.add(plus);
			
			}
			beItem = beItem.next;
		}while( beItem != null);{
			frame.pack();
			frame.setVisible(true);
		}
		
		

	}
	public void mouseClicked(MouseEvent event){  }
	public void mousePressed(MouseEvent event){  }
	public void mouseEntered(MouseEvent event){ 
		((TermLabel)(event.getSource())).setBackground(Color.yellow);
	}
	public void mouseExited(MouseEvent event){ 
		((TermLabel)(event.getSource())).setBackground(Color.white);
	}
	public void mouseReleased(MouseEvent event){  }
}
