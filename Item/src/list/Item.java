package list;

public class Item {
     public Object item;
     public Item next;
    public Item previous;
     
     public Item( Object anObject){
     	item = anObject;
     	next = null;
     	previous = null;
     	}
     
     public void insertBefore( Object anObject ){
     	Item newItem = new Item(anObject);
    	if( previous == null ){
    		previous = newItem;
    		newItem.next = this;
    	} else {
    		newItem.next = this;
    		newItem.previous = this.previous;

    		this.previous = newItem;
    		this.previous.previous.next = newItem;
        	}
     }
     
    public void insertAfter( Object anObject ){
     	Item newItem = new Item(anObject);
    	if( next == null ){
    		next = newItem;
    		newItem.previous = this;
    	}else {
    		newItem.previous = this;
    		newItem.next = this.next;
    		this.next = newItem;
    		this.next.next.previous = newItem;
    		
    	}
     }
    
    
   public Item first(){
    	Item current = this;
    	while( current.previous != null ){
    		current = current.previous;
    	}
    	return current;
    }
   
   
   public Item last(){
    	Item current = this;
    	while( current.next != null ){
    		current = current.next;
    	}
    	return current;
    	}

   public void deleteBefore( ){
    if (this.previous != null){
    	if (this.previous.previous != null){
    		this.previous=this.previous.previous;
    		this.previous.next=this;
    	}else{
    		this.previous=this;
    		
    	}
    }
 }
   public void deleteAfter(){
    	if (this.next!=null){
    		if(this.next.next != null){
    			this.next=this.next.next;
    			this.next.previous=this;
    		}else{
    			this.next=this;
    		}
    		}
    	}
    }

//this is a comment