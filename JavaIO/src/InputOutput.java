import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class InputOutput {
	static StringBuffer stringDataBuffer = new StringBuffer();
public	static String filename = null;
	static Scanner sc = new Scanner(System.in);
	
	
	public static void main(String[] args) {
		boolean fileRead = readFile();
		if (fileRead){
			replacement();
			writeToFile();
			
		}
		System.exit(0);
	}
	@SuppressWarnings("finally")
	private static boolean readFile(){
		System.out.println("Please enter your file location");
		filename = sc.nextLine();
		Scanner fileToRead = null;
		try{
			fileToRead = new Scanner(new File(filename));
			
			for(String line; fileToRead.hasNextLine() && (line = fileToRead.nextLine()) != null;){
				System.out.println(line);
				stringDataBuffer.append(line).append("\r\n");
			}
			fileToRead.close();
			return true;
		}catch (FileNotFoundException e){
			System.out.println("The file "+ filename + " could not be found" + e.getMessage());
			return false;
		}finally{
			fileToRead.close();
			return true;}
		}
		private static void writeToFile(){
			try{
				BufferedWriter buffWriter = new BufferedWriter(new FileWriter(filename));
				buffWriter.write(stringDataBuffer.toString());
					buffWriter.close();	
			}catch (Exception e){
				System.out.println("Error occured while writing to the file: " + e.getMessage());
			}
		}
		private static void replacement(){
			System.out.println("please enter teh contents of the line you would like to edit: ");
			String lineToEdit = sc.nextLine();
			System.out.println("Enter the replacement text: ");
			String replacementText = sc.nextLine();
			int startIndex = stringDataBuffer.indexOf(lineToEdit);
			int endIndex = startIndex + lineToEdit.length();
			stringDataBuffer.replace(startIndex, endIndex, replacementText);
			System.out.println("here is the new text: \n" + stringDataBuffer);
		
	}
}


